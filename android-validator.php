<?php

require __DIR__ . '/vendor/autoload.php';

use ReceiptValidator\GooglePlay\Validator as PlayValidator;

class AndroidValidator
{
    private $googleClient;
    private $googleAndroidPublisher;
    private $validator;

    function __construct()
    {
        $this->googleClient = new \Google_Client();
        $this->googleClient->setScopes([\Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);
        $this->googleClient->setApplicationName('purchase-validator');
        $this->googleClient->setAuthConfig('googleapi.json');
        $this->googleAndroidPublisher = new \Google_Service_AndroidPublisher($this->googleClient);
        $this->validator = new \ReceiptValidator\GooglePlay\Validator($this->googleAndroidPublisher);
    }

    function json_response($code = 200, $data = null)
    {
        header_remove();
        http_response_code($code);
        header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
        header('Content-Type: application/json');
        $status = array(
            200 => '200 OK',
            400 => '400 Bad Request',
            500 => '500 Internal Server Error'
        );
        header('Status: ' . $status[$code]);
        return json_encode(array(
            'status' => $code < 300,
            'data' => json_encode($data)
        ));
    }

    function verify($packageName, $productId, $purchaseToken)
    {
        try {
            $response = $this->validator->setPackageName($packageName)
                ->setProductId($productId)
                ->setPurchaseToken($purchaseToken)
                ->validatePurchase();
        } catch (\Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            return $this->json_response(500);
        }

        if (!empty($response)) {
            return $this->json_response(200, $response->getRawResponse());
        } else {
            return $this->json_response(400);
        }
    }
}


