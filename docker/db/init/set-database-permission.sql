USE mysql;
GRANT ALL PRIVILEGES ON `purchase-validator`.* TO 'root'@'%';
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'unitymobile';
UPDATE mysql.user SET host='%' WHERE user='root' AND host = 'localhost';
FLUSH PRIVILEGES;