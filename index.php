<?php
    header("Access-Control-Allow-Origin: *");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    include_once('android-validator.php');
    include_once('ios-validator.php');

    $os = $_GET['os'];

    if ($os == 'Android') {
        $packageName = $_GET['packageName'];
        $productId = $_GET['productId'];
        $purchaseToken = $_GET['purchaseToken'];
        $android = new AndroidValidator();
        echo $android->verify($packageName, $productId, $purchaseToken);
    } else if ($os == 'iOS') {
        $sandbox = $_GET['sandbox'];
        $base64Data = $_GET['base64Data'];
        $ios = new IOSValidator($sandbox);
        echo $ios->verify($base64Data);
    }



