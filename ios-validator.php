<?php

require __DIR__ . '/vendor/autoload.php';

use ReceiptValidator\iTunes\Validator as iTunesValidator;

class IOSValidator
{
    private $validator;

    function __construct($sandbox)
    {
        $this->validator = new iTunesValidator($sandbox ? iTunesValidator::ENDPOINT_SANDBOX : iTunesValidator::ENDPOINT_PRODUCTION);
    }

    function json_response($code = 200, $data = null)
    {
        header_remove();
        http_response_code($code);
        header("Cache-Control: no-transform,public,max-age=300,s-maxage=900");
        header('Content-Type: application/json');
        $status = array(
            200 => '200 OK',
            400 => '400 Bad Request',
            422 => 'Unprocessable Entity',
            500 => '500 Internal Server Error'
        );
        header('Status: ' . $status[$code]);
        return json_encode(array(
            'status' => $code < 300,
            'data' => $data
        ));
    }

    function verify($receiptBase64Data)
    {
        try {
            $response = $this->validator->setReceiptData($receiptBase64Data)->validate();
        } catch (Exception $e) {
            echo $e->getMessage() . PHP_EOL;
            return $this->json_response(500);
        }

        if (!empty($response)) {
            if ($response->isValid()) {
                return $this->json_response(200, $response->getReceipt());
            } else {
                return $this->json_response(400);
            }
        }
    }
}