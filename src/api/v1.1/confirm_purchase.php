<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../config/Common.php';
include_once '../../config/Utils.php';
include_once '../../config/Authenticate.php';
include_once '../../config/Database.php';
include_once '../../config/ResponseTemplate.php';
include_once '../../model/v1.1/Purchase.php';

Authenticate::require();

$id = isset($_POST['id']) ? $_POST['id'] : Utils::Die();
$store = isset($_POST['store']) ? $_POST['store'] : Utils::Die();
$bid = isset($_POST['bid']) ? $_POST['bid'] : Utils::Die();
$pid = isset($_POST['pid']) ? $_POST['pid'] : Utils::Die();
$order = isset($_POST['order']) ? $_POST['order'] : Utils::Die();

$database = new Database();
$conn = $database->getConnection();
$purchase = new Purchase($conn);
$purchase->id = $id;
if ($purchase->getSingleById()) {
    if ($purchase->purchase_state == PurchaseState::Purchased->name) {
        $rest = new ResponseTemplate(111);
        echo json_encode($rest->build());
    } else {
        $purchase->purchase_state = PurchaseState::Purchased->name;
        if ($purchase->confirm()) {
            $purchaseResponse = [
                "Id" => $purchase->id,
                "UserId" => $purchase->user_id,
                "Store" => $purchase->store,
                "OrderId" => $purchase->order_id,
                "BundleId" => $purchase->bundle_id,
                "ProductId" => $purchase->product_id,
                "ProductType" => $purchase->product_type,
                "PurchaseTime" => $purchase->purchase_time,
                "PurchaseState" => $purchase->purchase_state,
                "Sandbox" => $purchase->purchase_type == 'Sandbox' ? true : false,
                "ProviderResponse" => $purchase->provider_response
            ];

            $rest = new ResponseTemplate(200);
            echo json_encode($rest->buildArray($purchaseResponse));
        } else {
            $rest = new ResponseTemplate(400);
            echo json_encode($rest->build());
        }
    }
} else {
    $rest = new ResponseTemplate(400);
    echo json_encode($rest->build());
}
