<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

use Ramsey\Uuid\Uuid;

include_once '../../config/Common.php';
include_once '../../config/Utils.php';
include_once '../../config/Authenticate.php';
include_once '../../config/Database.php';
include_once '../../config/ResponseTemplate.php';
include_once '../../config/AndroidValidator.php';
include_once '../../config/IosValidator.php';
include_once '../../model/v1.1/App.php';
include_once '../../model/v1.1/Purchase.php';

Authenticate::require();

$aid = isset($_POST['app']) ? $_POST['app'] : Utils::Die();
$store = isset($_POST['store']) ? $_POST['store'] : Utils::Die();
$bid = isset($_POST['bid']) ? $_POST['bid'] : Utils::Die();
$pid = isset($_POST['pid']) ? $_POST['pid'] : Utils::Die();
$type = isset($_POST['type']) ? $_POST['type'] : Utils::Die();
$user = isset($_POST['user']) ? $_POST['user'] : null;
$order = isset($_POST['order']) ? $_POST['order'] : Utils::Die();
$receipt = isset($_POST['receipt']) ? $_POST['receipt'] : Utils::Die();

$database = new Database();
$conn = $database->getConnection();
$app = new App($conn);
$purchase = new Purchase($conn);

if ($store == 'GooglePlay') {
    $jsonReceipt = Utils::JsonDecode($receipt, true);
    if (!is_array($jsonReceipt) || count($jsonReceipt) <= 0) {
        Utils::Die();
        return;
    }

    if (array_key_exists('json', $jsonReceipt)) {
        $json = Utils::JsonDecode($jsonReceipt['json'], true);
    } else {
        Utils::Die();
        return;
    }

    if (array_key_exists('orderId', $json)) {
        $orderId = $json['orderId'];
    } else {
        Utils::Die();
        return;
    }

    if (array_key_exists('purchaseToken', $json)) {
        $token = $json['purchaseToken'];
    } else {
        Utils::Die();
        return;
    }

    if (array_key_exists('purchaseTime', $json)) {
        $purchase->purchase_time = Utils::ParseMillisecondUnixTimestamp($json['purchaseTime']);
    } else {
        Utils::Die();
        return;
    }

    $purchase->store = $store;
    $purchase->bundle_id = $bid;
    $purchase->order_id = $orderId;
    $purchase->product_id = $pid;
    $purchase->product_type = $type;

    if ($purchase->existsOrderPurchased()) {
        $purchaseResponse = [
            "Id" => $purchase->id,
            "UserId" => $purchase->user_id,
            "Store" => $purchase->store,
            "OrderId" => $purchase->order_id,
            "BundleId" => $bid,
            "ProductId" => $purchase->product_id,
            "ProductType" => $purchase->product_type,
            "PurchaseTime" => $purchase->purchase_time,
            "PurchaseState" => $purchase->purchase_state,
            "Sandbox" => $purchase->purchase_type == 'Sandbox' ? true : false
        ];

        $rest = new ResponseTemplate(110);
        echo json_encode($rest->buildArray($purchaseResponse));
        return;
    }

    $app->id = $aid;
    if ($app->getSingleById()) {
        $purchase->id = Uuid::uuid4();
        $purchase->user_id = Utils::IsNullOrEmptyString($user) ? Uuid::uuid4() : $user;
        $purchaseEnv = StoreEnvironment::Production;
        $purchaseState = PurchaseState::Pending;
        $androidValidator = new AndroidValidator($app->name, json_decode($app->acc_google, true));
        $purchaseProduct = $androidValidator->verify($purchase->bundle_id, $purchase->product_id, $token);
        if (!is_null($purchaseProduct)) {
            $jsonPurchaseProduct = Utils::JsonDecode(json_encode($purchaseProduct), true);
            if (!is_array($jsonPurchaseProduct) || count($jsonPurchaseProduct) <= 0) {
                Utils::Die();
                return;
            }

            if (array_key_exists('purchaseType', $jsonPurchaseProduct)) {
                if (!is_null($jsonPurchaseProduct['purchaseType'])) {
                    switch ($jsonPurchaseProduct['purchaseType']) {
                        case 0:
                            $purchaseEnv = StoreEnvironment::Sandbox;
                            break;
                        case 1:
                            $purchaseEnv = StoreEnvironment::PromoCode;
                            break;
                        case 2:
                            $purchaseEnv = StoreEnvironment::Rewarded;
                            break;
                    }
                }
            }

            $purchase->order_id = $jsonPurchaseProduct['orderId'];
            $purchase->purchase_time = Utils::ParseMillisecondUnixTimestamp($jsonPurchaseProduct['purchaseTimeMillis']);
            $purchase->provider_response = json_encode($purchaseProduct);
        }

        $purchase->purchase_type = $purchaseEnv->name;
        $purchase->purchase_state = $purchaseState->name;

        if ($purchase->create()) {
            $purchaseResponse = [
                "Id" => $purchase->id,
                "UserId" => $purchase->user_id,
                "Store" => $purchase->store,
                "OrderId" => $purchase->order_id,
                "BundleId" => $purchase->bundle_id,
                "ProductId" => $purchase->product_id,
                "ProductType" => $purchase->product_type,
                "PurchaseTime" => $purchase->purchase_time,
                "PurchaseState" => $purchaseState->name,
                "Sandbox" => $purchaseEnv->value == 0 ? true : false,
                "ProviderResponse" => $purchase->provider_response
            ];

            $rest = new ResponseTemplate(200);
            echo json_encode($rest->buildArray($purchaseResponse));
        } else {
            $rest = new ResponseTemplate(109);
            echo json_encode($rest->build());
        }
    } else {
        $rest = new ResponseTemplate(404);
        echo json_encode($rest->build());
    }
} else if ($store == 'AppleAppStore') {
    $purchase->id = Uuid::uuid4();
    $purchase->user_id = Utils::IsNullOrEmptyString($user) ? Uuid::uuid4() : $user;
    $purchase->store = $store;
    $purchase->bundle_id = $bid;
    $purchase->order_id = $order;
    $purchase->product_id = $pid;
    $purchase->product_type = $type;

    if ($purchase->existsOrderPurchased()) {
        $purchaseResponse = [
            "Id" => $purchase->id,
            "UserId" => $purchase->user_id,
            "Store" => $purchase->store,
            "OrderId" => $purchase->order_id,
            "BundleId" => $bid,
            "ProductId" => $purchase->product_id,
            "ProductType" => $purchase->product_type,
            "PurchaseTime" => $purchase->purchase_time,
            "PurchaseState" => $purchase->purchase_state,
            "Sandbox" => $purchase->purchase_type == 'Sandbox' ? true : false
        ];

        $rest = new ResponseTemplate(110);
        echo json_encode($rest->buildArray($purchaseResponse));
        return;
    }

    $app->id = $aid;
    if ($app->getSingleById()) {
        $iosValidator = new IosValidator($app->acc_apple);
        $purchaseProduct = $iosValidator->verify($receipt);
        if (!is_null($purchaseProduct)) {
            $purchaseEnv = StoreEnvironment::Production;
            $purchaseState = PurchaseState::Pending;
            if ($purchaseProduct->isSandbox()) {
                $purchaseEnv = StoreEnvironment::Sandbox;
            }

            $isMatch = false;
            foreach ($purchaseProduct->getPurchases() as $p) {
                if ($p->getTransactionId() == $purchase->order_id) {
                    $rawData = $p->getRawResponse();
                    if ($rawData != null) {
                        $isMatch = true;
                        $purchase->order_id = $rawData['transaction_id'];
                        $purchase->purchase_time = Utils::ParseMillisecondUnixTimestamp($rawData['purchase_date_ms']);
                    }
                }
            }

            if (!$isMatch) {
                $rest = new ResponseTemplate(107);
                echo json_encode($rest->build());
                return;
            }

            $purchase->purchase_type = $purchaseEnv->name;
            $purchase->purchase_state = $purchaseState->name;
            $purchase->provider_response = json_encode($purchaseProduct->getRawData());

            if ($purchase->create()) {
                $purchaseResponse = [
                    "Id" => $purchase->id,
                    "UserId" => $purchase->user_id,
                    "Store" => $purchase->store,
                    "OrderId" => $purchase->order_id,
                    "BundleId" => $purchase->bundle_id,
                    "ProductId" => $purchase->product_id,
                    "ProductType" => $purchase->product_type,
                    "PurchaseTime" => $purchase->purchase_time,
                    "PurchaseState" => $purchaseState->name,
                    "Sandbox" => $purchaseEnv->value == 0 ? true : false,
                    "ProviderResponse" => $purchase->provider_response
                ];

                $rest = new ResponseTemplate(200);
                echo json_encode($rest->buildArray($purchaseResponse));
            } else {
                $rest = new ResponseTemplate(109);
                echo json_encode($rest->build());
            }
        } else {
            $rest = new ResponseTemplate(106);
            echo json_encode($rest->build());
        }
    } else {
        $rest = new ResponseTemplate(105);
        echo json_encode($rest->build());
    }
} else {
    $rest = new ResponseTemplate(108);
    echo json_encode($rest->build());
}
