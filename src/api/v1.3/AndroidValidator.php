<?php

require __DIR__ . '/../vendor/autoload.php';

use ReceiptValidator\GooglePlay\Validator as PlayValidator;

class AndroidValidator {
    public $validator;

    function __construct($app_name, $acc_google) {
        $client = new \Google_Client();
        $client->setScopes([\Google\Service\AndroidPublisher::ANDROIDPUBLISHER]);
        $client->setApplicationName($app_name);
        $client->setAuthConfig($acc_google);
        $publisher = new \Google\Service\AndroidPublisher($client);
        $this->validator = new \ReceiptValidator\GooglePlay\Validator($publisher);
        return $this->validator;
    }

    public function verify($bid, $pid, $ptype, $token) {
        try {
            if ($ptype == ProductType::Subscription->name) {
                $response = $this->validator->setPackageName($bid)
                    ->setProductId($pid)
                    ->setPurchaseToken($token)
                    ->validateSubscription();
            } else {
                $response = $this->validator->setPackageName($bid)
                    ->setProductId($pid)
                    ->setPurchaseToken($token)
                    ->validatePurchase();
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            return null;
        }

        return $response->getRawResponse();
    }

    public function list($bid) {
        try {
            $response = $this->validator->setPackageName($bid)
                ->voidedPurchases();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            return null;
        }

        return $response;
    }
}
