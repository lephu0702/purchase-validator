<?php

require __DIR__ . '/../../vendor/autoload.php';

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../config/Utils.php';
include_once '../../config/Authenticate.php';
include_once '../../config/Database.php';
include_once '../../config/AndroidValidator.php';
include_once '../../model/v1/App.php';

Authenticate::require();

$name = isset($_POST['name']) ? $_POST['name'] : null;
$user_mode = isset($_POST['user_mode']) ? $_POST['user_mode'] : 0;
$bid_google = isset($_POST['bid_google']) ? $_POST['bid_google'] : null;
$bid_apple = isset($_POST['bid_apple']) ? $_POST['bid_apple'] : null;
$acc_google = isset($_POST['acc_google']) ? $_POST['acc_google'] : null;
$acc_apple = isset($_POST['acc_apple']) ? $_POST['acc_apple'] : null;

$database = new Database();
$conn = $database->getConnection();
$app = new App($conn);

$app->id = Utils::GenerateKey();
$app->user_mode = $user_mode;

if (!Utils::IsNullOrEmptyString($name)) {
    $app->name = $name;
}

if (!Utils::IsNullOrEmptyString($bid_google)) {
    $app->bid_google = $bid_google;
}

if (!Utils::IsNullOrEmptyString($bid_apple)) {
    $app->bid_apple = $bid_apple;
}

if (!Utils::IsNullOrEmptyString($acc_google)) {
    $app->acc_google = $acc_google;
}

if (!Utils::IsNullOrEmptyString($acc_apple)) {
    $app->acc_apple = $acc_apple;
}

if ($app->create()) {
    echo 'app ' . $name . '(' . $app->id . ')' . ' created successfully.';
} else {
    echo 'app ' . $name . ' could not be created.';
}
