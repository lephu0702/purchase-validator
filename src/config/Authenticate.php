<?php
class Authenticate {
    public static $authToken = "YWRtaW46dW5pbW9iLmNvbS52bg==";

    static function require() {
        $token = Authenticate::getBearerToken();
        if (($token === null || trim($token) === '') || trim($token) != Authenticate::$authToken) {
            header('HTTP/1.1 401 Authorization Required');
            echo json_encode([
                "status" => 401,
                "message" => 'Unauthorized.',
                "data" => null
            ]);
            exit;
        }
    }

    /**
     * Get hearder Authorization
     **/
    static function getAuthorizationHeader() {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /**
     * get access token from header
     * */
    static function getBearerToken() {
        $headers = Authenticate::getAuthorizationHeader();
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
}
