<?php
enum StoreEnvironment: int {
    case Unknow = -1;
    case Sandbox = 0;
    case PromoCode = 1;
    case Rewarded = 2;
    case Production = 3;
}

enum ProductType: int {
    case Unknow = -1;
    case Consumable = 0;
    case NonConsumable = 1;
    case Subscription = 2;
}

enum PurchaseState: int {
    case Unknow = -1;
    case Purchased = 0;
    case Canceled = 2;
    case Pending = 3;
}
