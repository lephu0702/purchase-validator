<?php
class Database {
    public $conn;

    public function getConnection() {
        $host = getenv('DB_HOST', true) ?: getenv('DB_HOST');
        $username = getenv('DB_USERNAME', true) ?: getenv('DB_USERNAME');
        $password  = getenv('DB_PASSWORD', true) ?: getenv('DB_PASSWORD');
        $database_name = getenv('DB_DATABASE', true) ?: getenv('DB_DATABASE');
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => false
        ];
        $this->conn = null;
        try {
            $this->conn = new PDO("mysql:host=" . $host . ";dbname=" . $database_name . ";charset=utf8mb4", $username, $password, $options);
            // $this->conn->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Database could not be connected: " . $exception->getMessage();
        }
        return $this->conn;
    }

    public function closeConnection() {
        $this->conn = null;
    }
}
