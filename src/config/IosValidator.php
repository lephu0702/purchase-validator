<?php

require __DIR__ . '/../vendor/autoload.php';

use ReceiptValidator\iTunes\Validator as iTunesValidator;

class IosValidator {
    private $validator;
    private $sharedSecret;

    function __construct($shared_secret) {
        $this->sharedSecret = $shared_secret;
        $this->validator = new iTunesValidator(iTunesValidator::ENDPOINT_PRODUCTION);
    }

    function verify($base64Data) {
        try {
            $response = $this->validator->setSharedSecret($this->sharedSecret)->setReceiptData($base64Data)->validate();
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

        if ($response->isValid()) {
            return $response;
        } else {
            return null;
        }
    }
}
