<?php

class ResponseTemplate {
    /**
     * @see https://developer.mozilla.org/docs/Web/HTTP/Status
     */
    const STATUS_MAP = [
        /* informational responses */
        100 => "Continue",
        101 => "Switching Protocol",
        102 => "Processing",
        103 => "Early Hints",
        104 => "Version Code Invalid",
        105 => "Invalid App ID",
        106 => "Receipt Validation Failed",
        107 => "Transaction ID does not match",
        108 => "Unsupported Store",
        109 => "Unexpected Response",
        110 => "Duplicate Transaction",
        111 => "Purchased",
        /* successful responses */
        200 => "Ok",
        201 => "Created",
        202 => "Accepted",
        203 => "Non-Authoritative Information",
        204 => "No Content",
        205 => "Reset Content",
        206 => "Partial Content",
        207 => "Multi-Status",
        208 => "Multi-Status",
        226 => "IM Used",
        /* redirects */
        300 => "Multiple Choice",
        301 => "Moved Permanently",
        302 => "Found",
        303 => "See Other",
        304 => "Not Modified",
        305 => "Use Proxy",
        306 => "unused ",
        307 => "Temporary Redirect",
        308 => "Permanent Redirect",
        /* client errors */
        400 => "Bad Request",
        401 => "Unauthorized",
        402 => "Payment Required",
        403 => "Forbidden",
        404 => "Not Found",
        405 => "Method Not Allowed",
        406 => "Not Acceptable",
        407 => "Proxy Authentication Required",
        408 => "Request Timeout",
        409 => "Conflict",
        410 => "Gone",
        411 => "Length Required",
        412 => "Precondition Failed",
        413 => "Payload Too Large",
        414 => "URI Too Long",
        415 => "Unsupported Media Type",
        416 => "Requested Range Not Satisfiable",
        417 => "Expectation Failed",
        418 => "I'm a teapot",
        421 => "Misdirected Request",
        422 => "Unprocessable Entity",
        423 => "Locked",
        424 => "Failed Dependency",
        425 => "Too Early",
        426 => "Upgrade Required",
        428 => "Precondition Required",
        429 => "Too Many Requests",
        431 => "Request Header Fields Too Large",
        451 => "Unavailable For Legal Reasons",
        /* server errors */
        500 => "Internal Server Error",
        501 => "Not Implemented",
        502 => "Bad Gateway",
        503 => "Service Unavailable",
        504 => "Gateway Timeout",
        505 => "HTTP Version Not Supported",
        506 => "Variant Also Negotiates",
        507 => "Insufficient Storage",
        508 => "Loop Detected",
        510 => "Not Extended",
        511 => "Network Authentication Required",
    ];

    const STATUS_CODE_EXCEPTION_MESSAGE = 'The HTTP status code must be one of the following: 100, 101, 102, 103, 200, 201, 202, 203, 204, 205, 206, 207, 208, 226, 300, 301, 302, 303, 304, 305, 306, 307, 308, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 421, 422, 423, 424, 425, 426, 428, 429, 431, 451, 500, 501, 502, 503, 504, 505, 506, 507, 508, 510, 511.';
    const ARG_TYPE_EXCEPTION_MESSAGE = 'Given argument missmatches with required type.';

    /**
     * @var int
     */
    private $statusCode;

    function __construct(int $statusCode) {
        if (!in_array($statusCode, array_keys(self::STATUS_MAP), true)) throw new Exception(self::STATUS_CODE_EXCEPTION_MESSAGE);
        $this->statusCode = $statusCode;
    }

    /**
     * Builds an associative array based on ResponseTemplate configuration, status code and given data and links.
     *
     * @param Type $data Data to be attached to data property of response template.
     * @return Description
     */
    public function buildArray(array $data = []): array {
        if (!is_array($data)) throw new Exception(self::ARG_TYPE_EXCEPTION_MESSAGE);
        return [
            "status" => $this->statusCode,
            "message" => $this->getStatusMessage() . '.',
            "data" => (count($data) > 0 ? $data : null)
        ];
    }

    public function build($data = null): array {
        return [
            "status" => $this->statusCode,
            "message" => $this->getStatusMessage() . '.',
            "data" => (!Utils::IsNullOrEmptyString($data) ? $data : null)
        ];
    }

    /**
     * Get HTTP status message accordingly given status code.
     *
     * @return HTTP Message associated to given status code.
     */
    public function getStatusMessage(): string {
        return self::STATUS_MAP[$this->statusCode];
    }
}
