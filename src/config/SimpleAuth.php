<?php
class SimpleAuth {
    public static $auth_user = "admin";
    public static $auth_pass = "unimob.com.vn";

    public static function require_auth() {
        header('Cache-Control: no-cache, must-revalidate, max-age=0');

        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));

        $is_not_authenticated = (!$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != SimpleAuth::$auth_user ||
            $_SERVER['PHP_AUTH_PW']   != SimpleAuth::$auth_pass
        );

        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            echo json_encode([
                "status" => 401,
                "message" => 'Access denied.',
                "data" => null
            ]);
            exit;
        }
    }
}
