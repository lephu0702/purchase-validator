<?php
class Utils {
    public static function IsNullOrEmptyString($str) {
        return ($str === null || trim($str) === '');
    }

    public static function GenerateKey($length = 8) {
        $stringSpace = '0123456789abcdefghijklmnopqrstuvwxyz';
        $pieces = [];
        $max = mb_strlen($stringSpace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $pieces[] = $stringSpace[random_int(0, $max)];
        }
        return implode('', $pieces);
    }

    /**
     * Remove the first and last quote from a quoted string of text
     *
     * @param mixed $text
     */
    public static function StripQuotes($text) {
        return preg_replace('/^(\'[^\']*\'|"[^"]*")$/', '$2$3', $text);
    }

    /**
     * Parse the JSON response body and return an array
     *
     * @return array|string|int|bool|float
     * @throws RuntimeException if the response body is not in JSON format
     */
    public static function JsonDecode($body) {
        $data = json_decode((string) $body, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new RuntimeException('Unable to parse response body into JSON: ' . json_last_error());
        }

        return $data === null ? array() : $data;
    }

    public static function Die() {
        http_response_code(400);
        $rest = new ResponseTemplate(400);
        echo json_encode($rest->build());
        die();
    }

    public static function ParseMillisecondUnixTimestamp($time) {
        return date("Y-m-d H:i:s", (int)($time / 1000));
    }
}
