<?php
class App {
    private $conn;
    private $db_table = "app";
    public $id;
    public $name;
    public $user_mode;
    public $bid_google;
    public $bid_apple;
    public $acc_google;
    public $acc_apple;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function getAll() {
        $sqlQuery = "SELECT * FROM " . $this->db_table . "";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->execute();
        return $stmt;
    }

    public function getSingleById() {
        try {
            $query = "SELECT * FROM " . $this->db_table . " WHERE id = :id LIMIT 0,1";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $this->id);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $this->name = $row['name'];
                $this->user_mode = $row['user_mode'];
                $this->bid_apple = $row['bid_apple'];
                $this->acc_google = $row['acc_google'];
                $this->acc_apple = $row['acc_apple'];
                return true;
            }
            return false;
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    public function getSingleByAndroidBid() {
        try {
            $query = "SELECT * FROM " . $this->db_table . " WHERE bid_google = :bid_google LIMIT 0,1";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':bid_google', $this->bid_google);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $this->id = $row['id'];
                $this->name = $row['name'];
                $this->user_mode = $row['user_mode'];
                $this->bid_apple = $row['bid_apple'];
                $this->acc_google = $row['acc_google'];
                $this->acc_apple = $row['acc_apple'];
                return true;
            }
            return false;
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    public function getSingleByAppleBid() {
        try {
            $query = "SELECT * FROM " . $this->db_table . " WHERE bid_apple = :bid_apple LIMIT 0,1";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':bid_apple', $this->bid_apple);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $this->id = $row['id'];
                $this->name = $row['name'];
                $this->user_mode = $row['user_mode'];
                $this->bid_google = $row['bid_google'];
                $this->acc_google = $row['acc_google'];
                $this->acc_apple = $row['acc_apple'];
            }
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    public function create() {
        try {
            $query = "INSERT INTO " . $this->db_table . " SET
            id = :id,
            name = :name,
            user_mode = :user_mode,
            bid_google = :bid_google,
            bid_apple = :bid_apple,
            acc_google = :acc_google,
            acc_apple = :acc_apple";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':name', $this->name);
            $stmt->bindParam(':user_mode', $this->user_mode);
            $stmt->bindParam(':bid_google', $this->bid_google);
            $stmt->bindParam(':bid_apple', $this->bid_apple);
            $stmt->bindParam(':acc_google', $this->acc_google);
            $stmt->bindParam(':acc_apple', $this->acc_apple);
            if ($stmt->execute()) {
                return true;
            }
            return false;
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    public function updateEmployee() {
        $sqlQuery = "UPDATE " . $this->db_table . " SET name = :name WHERE id = :id";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(":name", $this->name);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }

    public function deleteEmployee() {
        $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id = ?";
        $stmt = $this->conn->prepare($sqlQuery);
        $stmt->bindParam(1, $this->id);
        if ($stmt->execute()) {
            return true;
        }
        return false;
    }
}
