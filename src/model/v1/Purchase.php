<?php
class Purchase {
    private $conn;
    private $db_table = "purchase";
    public $id;
    public $user_id;
    public $store;
    public $order_id;
    public $bundle_id;
    public $product_id;
    public $product_type;
    public $purchase_time;
    public $purchase_state;
    public $purchase_type;
    public $provider_response;
    public $created_at;
    public $updated_at;

    public function __construct($db) {
        $this->conn = $db;
    }

    public function getSingleById() {
        try {
            $query = "SELECT * FROM " . $this->db_table . " WHERE id = :id LIMIT 0,1";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $this->id);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($row) {
                    $this->user_id = $row["user_id"];
                    $this->store = $row["store"];
                    $this->order_id = $row["order_id"];
                    $this->bundle_id = $row["bundle_id"];
                    $this->product_id = $row["product_id"];
                    $this->product_type = $row["product_type"];
                    $this->purchase_time = $row["purchase_time"];
                    $this->purchase_state = $row["purchase_state"];
                    $this->purchase_type = $row["purchase_type"];
                    $this->provider_response = $row["provider_response"];
                    return true;
                }
            }
            return false;
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    public function existsOrderPurchased() {
        try {
            $query = "SELECT purchase_state FROM " . $this->db_table . " WHERE store = :store AND bundle_id = :bundle_id AND product_id = :product_id AND order_id = :order_id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':store', $this->store);
            $stmt->bindParam(':bundle_id', $this->bundle_id);
            $stmt->bindParam(':product_id', $this->product_id);
            $stmt->bindParam(':order_id', $this->order_id);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($row) {
                    $purchaseState = $row["purchase_state"];
                    if ($purchaseState == "Purchased") {
                        return true;
                    }
                }
            }
            return false;
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    public function create() {
        try {
            $query = "INSERT INTO " . $this->db_table . " SET
            id = :id,
            user_id = :user_id,
            store = :store,
            order_id = :order_id,
            bundle_id = :bundle_id,
            product_id = :product_id,
            product_type = :product_type,
            purchase_type = :purchase_type,
            purchase_time = :purchase_time,
            purchase_state = :purchase_state,
            provider_response = :provider_response";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':user_id', $this->user_id);
            $stmt->bindParam(':store', $this->store);
            $stmt->bindParam(':order_id', $this->order_id);
            $stmt->bindParam(':bundle_id', $this->bundle_id);
            $stmt->bindParam(':product_id', $this->product_id);
            $stmt->bindParam(':product_type', $this->product_type);
            $stmt->bindParam(':purchase_type', $this->purchase_type);
            $stmt->bindParam(':purchase_time', $this->purchase_time);
            $stmt->bindParam(':purchase_state', $this->purchase_state);
            $stmt->bindParam(':provider_response', $this->provider_response);
            if ($stmt->execute()) {
                return true;
            }
            return false;
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }

    public function confirm() {
        try {
            $query = "UPDATE " . $this->db_table . " SET purchase_state = :purchase_state WHERE id = :id AND store = :store AND bundle_id = :bundle_id AND product_id = :product_id AND order_id = :order_id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':store', $this->store);
            $stmt->bindParam(':bundle_id', $this->bundle_id);
            $stmt->bindParam(':product_id', $this->product_id);
            $stmt->bindParam(':order_id', $this->order_id);
            $stmt->bindParam(':purchase_state', $this->purchase_state);
            if ($stmt->execute()) {
                if ($stmt->rowCount() >= 1) {
                    return true;
                }
            }
            return false;
        } catch (PDOException $e) {
            var_dump($e->getMessage());
        }
    }
}
